package com.torus.zounypasajero;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by chas on 13/03/2017.
 */

public class AdaptadorItemDireccion extends BaseAdapter {

    private Context context;
    private List<Direccion> direcciones;

    public AdaptadorItemDireccion(Context context, List<Direccion> direcciones) {
        this.context = context;
        this.direcciones = direcciones;
    }

    @Override
    public int getCount() {
        return this.direcciones.size();
    }

    @Override
    public Object getItem(int i) {
        return this.direcciones.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View rowView = view;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.lista_lugares_item, viewGroup, false);
        }

        TextView tvCalle = (TextView) rowView.findViewById(R.id.tvLugar);
        TextView tvCiudad = (TextView) rowView.findViewById(R.id.tvCiudad);

        Direccion dir = this.direcciones.get(i);
        tvCalle.setText(dir.getCalle());
        tvCiudad.setText(dir.getCiudad());

        return rowView;
    }
}
