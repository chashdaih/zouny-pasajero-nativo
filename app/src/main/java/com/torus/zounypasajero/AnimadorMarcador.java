package com.torus.zounypasajero;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.util.Log;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;

/**
 * Created by chas on 31/03/2017.
 */

public class AnimadorMarcador {

    private static final String TAG = AnimadorMarcador.class.getSimpleName();

    protected float prevRot = 0;
    protected int paso = 0;
    protected ArrayList <LatLng> directionPositionList;
    protected Marker marker;
    protected int duracion = 5000;

    public AnimadorMarcador(Marker marcador, ArrayList<LatLng> puntos) {
        this.marker = marcador;
        this.directionPositionList = puntos;
    }

    public AnimadorMarcador(Marker marcador, ArrayList<LatLng> puntos, int duracion) {
        this.marker = marcador;
        this.directionPositionList = puntos;
        this.duracion = duracion;
    }


    private void animateMarker(final LatLng destination, final Marker marker) {
            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = destination;

            //final float startRotation = marker.getRotation();
            final float angle = (float)angle(startPosition.latitude, startPosition.longitude,
                    endPosition.latitude, endPosition.longitude);


            final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0,1);
            valueAnimator.setDuration(1000);
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    try {
                        float v = valueAnimator.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
                        //marker.setRotation(computeRotation(v, startRotation, destination.getBearing()));
                        //if (angle != 0f) {
                        marker.setRotation(computeRotation(v, prevRot, angle));
                        //}
                    } catch (Exception ex){
                        Log.i(TAG, "ocurrio algo :O "+ ex);
                    }
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    prevRot = angle;
                    animarDeNuevo();

                }
            });
            valueAnimator.start();
    }

    public void animarDeNuevo() {
        int numeroVueltas = directionPositionList.size();
        if (paso < numeroVueltas - 1) {
            //int len = directionPositionList.size();
            LatLng desti = new LatLng(directionPositionList.get(paso).latitude,
                    directionPositionList.get(paso).longitude);
            animateMarker(desti, marker);
            paso++;
        } else {
            Log.i(TAG, "Fin de la animación");
            marker.remove();
        }
    }

    private float computeRotation(float fraction, float start, float end) {
        float normalizeEnd = end - start; // rotate start to 0
        float normalizedEndAbs = (normalizeEnd + 360) % 360;

        float direction = (normalizedEndAbs > 180) ? -1 : 1; // -1 = anticlockwise, 1 = clockwise
        float rotation;
        if (direction > 0) {
            rotation = normalizedEndAbs;
        } else {
            rotation = normalizedEndAbs - 360;
        }
        float result = fraction * rotation + start;
        return (result + 360) % 360;
    }

    private double angle(double cx, double cy, double ex, double ey ) {
        double dy = ey - cy;
        double dx = ex - cx;
        double theta = Math.atan2(dy, dx); // range (-PI, PI]
        theta *= 180 / Math.PI; // rads to degs, range (-180,180]
        return theta;
    }

    private interface LatLngInterpolator {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolator {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }
}
