package com.torus.zounypasajero;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;

public class SeleccionarPago extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccionar_pago);

        agregarToolbar();
    }

    private void agregarToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            // Poner ícono del drawer toggle
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            ab.setTitle("Seleccionar pago");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void rowClicked(View v) {
        RadioButton mujer = (RadioButton) findViewById(R.id.radioMujer);
        RadioButton hombre = (RadioButton) findViewById(R.id.radioHombre);
        switch (v.getId()) {
            case R.id.rlMujer:
                mujer.setChecked(true);
                hombre.setChecked(false);
                break;
            case R.id.rlHombre:
                mujer.setChecked(false);
                hombre.setChecked(true);
                break;
        }
    }

    public void onRadioButtonClicked(View v) {
        RadioButton mujer = (RadioButton) findViewById(R.id.radioMujer);
        RadioButton hombre = (RadioButton) findViewById(R.id.radioHombre);
        switch (v.getId()) {
            case R.id.radioMujer:
                hombre.setChecked(false);
                break;
            case R.id.radioHombre:
                mujer.setChecked(false);
                break;
        }
    }

    public void agregarTarjeta(View v) {
        startActivity(new Intent(this, AgregarTarjeta.class));
    }
}
