package com.torus.zounypasajero;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.model.Leg;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MapViewFragment extends Fragment
        implements ActivityCompat.OnRequestPermissionsResultCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        SlideToUnlock.OnSlideToUnlockEventListener {

    private static final String TAG = MapViewFragment.class.getSimpleName();
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    private static final int PLACE_PICKER_REQUEST = 3;

    protected String serverKey = "AIzaSyBtPWZIQvT5FT0VXbhi3UjVKwdAqpdKE08";

    MapView mMapView;
    public GoogleMap googleMap;
    protected GoogleApiClient mGoogleApiClient;

    private LatLng currentLocation;

    protected Location mLastLocation;
    private RelativeLayout cajaSwipe;
    private LinearLayout fondo;
    private ListView listaPuntos;
    private LinearLayout cajaPago;

    private FloatingActionButton myLocFab;

    public Marker cochePorSolicitante;
    protected Marker marcadorInicial;
    protected Marker marcadorDestino;
    public LatLngBounds bounds;

    public ArrayList<Marker> cochesRandom = new ArrayList<Marker>();
    protected ArrayList<LatLng> directionPositionList;
    public List<Polyline> polArray = new ArrayList<Polyline>();

    //int paso=0;
    int numeroVueltas;
    private float prevRot;

    HandlerThread mHandlerThread;
    Handler mThreadHandler;
    AdaptadorItemDireccion mAdapter;
    PlaceAPI mPlaceAPI;
    private ArrayList<Direccion> resultadosAutoComplete;

    private SlideToUnlock slideToUnlockView;

    List<Direccion> direcciones = new ArrayList<Direccion>();

    // nueva caja arriba
    private View circuloAzul;
    private View circuloRosa;
    private ImageView lupa;
    private TextView tvADonde;
    private EditText etBuscarOrigen;
    private EditText etBuscarDestino;
    private RelativeLayout cajaPoli;
    private ImageView iGeneroPago;

    private ImageView marcadorFijo;
    private LinearLayout cajaMarcador;
    private Button botonMarcador;

    private String dirOrigen="";
    private String dirDestino;
    private LatLng latLngOrigen;
    private LatLng latLngDestino;

    private String busquedaOrigen = "";
    public String direccionOrigen = "";
    private String busquedaDestino = "";

    private Boolean noPresionadoLargo = true;
    private Boolean buscandoDestino = true;
    private Boolean esInicial = true;

    private AddressResultReceiver mResultReceiver;
    protected String mAddressOutput;
    //protected Marker marcadorArrastrado;
    //protected Marker marcadorPresionado;
    protected Marker marcadorModificado;

    public MapViewFragment() {
        if (mThreadHandler == null) {
            mHandlerThread = new HandlerThread(TAG, android.os.Process.THREAD_PRIORITY_BACKGROUND);
            mHandlerThread.start();

            mPlaceAPI = new PlaceAPI();

            mThreadHandler = new Handler(mHandlerThread.getLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    if (msg.what == 1) {
                        // Si ya existen resultados usarlos
                        //ArrayList<String> results = mPlaceAPI.autocomplete()
                    }
                }
            };
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main_menu, menu);
        final MenuItem myswitch = menu.findItem(R.id.myswitch);
        final SwitchCompat actionView = (SwitchCompat) myswitch.getActionView().findViewById(R.id.switchForActionBar);
        actionView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.i(TAG, "Estado: " + b);
                if (b) {
                    iGeneroPago.setImageResource(R.drawable.cara_azul);
                } else {
                    iGeneroPago.setImageResource(R.drawable.cara_rosa);
                }
            }
        });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mResultReceiver = new AddressResultReceiver(new Handler());

        final View rootView = inflater.inflate(R.layout.fragment_map_view, container, false);


        listaPuntos = (ListView) rootView.findViewById(R.id.lista_dir);
        LayoutInflater myInflater = getActivity().getLayoutInflater();
        RelativeLayout miCabecera = (RelativeLayout) myInflater.inflate(R.layout.cabecera, listaPuntos, false);
        listaPuntos.addHeaderView(miCabecera, null, true);
        listaPuntos.setAdapter(new AdaptadorItemDireccion(getActivity(), direcciones));

        buildGoogleApiClient();

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                try {// Intenta poner el estilo
                    boolean success = googleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.style_json)
                    );
                    if (!success) {
                        Log.e(TAG, "Style parsing failed");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e(TAG, "Can't find style. Error: ", e);
                }

                googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng arg0) {
                        ocultarLista();
                    }
                });

                googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                    LatLng latLng;
                    @Override
                    public void onCameraIdle() {
                        CameraPosition cameraPosition = googleMap.getCameraPosition();
                        latLng = cameraPosition.target;
                        if (marcadorFijo.getVisibility() == View.VISIBLE) {
                            if (buscandoDestino) {
                                latLngDestino = latLng;

                            } else {
                                latLngOrigen = latLng;
                            }
                            iniciarIntentDirecciones(latLng);
                        }
                    }
                });

            }
        });

        myLocFab = (FloatingActionButton) rootView.findViewById(R.id.fabMyLoc);
        myLocFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUpMap();
            }
        });

        slideToUnlockView = (SlideToUnlock) rootView.findViewById(R.id.slideToUnlock);
        slideToUnlockView.setExternalListener(this);


        fondo = (LinearLayout) rootView.findViewById(R.id.fondo);
        final RelativeLayout selCon = (RelativeLayout) rootView.findViewById(R.id.rlSelCon);
        final RelativeLayout rlFormaPago = (RelativeLayout) rootView.findViewById(R.id.rlFormaPago);
        final RelativeLayout cajaEmergencia = (RelativeLayout) rootView.findViewById(R.id.abajo_emergencia);
        final RelativeLayout cajaPoliInterna = (RelativeLayout) rootView.findViewById(R.id.caja_poli_interna);
        cajaPoli = (RelativeLayout) rootView.findViewById(R.id.caja_poli);
        cajaSwipe = (RelativeLayout) rootView.findViewById(R.id.caja_swipe);
        // nueva caja superior
        circuloAzul = rootView.findViewById(R.id.circulo_azul_nuevo);
        circuloRosa = rootView.findViewById(R.id.circulo_rosa_nuevo);
        lupa = (ImageView) rootView.findViewById(R.id.lupa_nuevo);
        tvADonde = (TextView) rootView.findViewById(R.id.tv_arriba);
        etBuscarOrigen = (EditText) rootView.findViewById(R.id.et_buscar_origen);
        etBuscarDestino = (EditText) rootView.findViewById(R.id.et_buscar_destino);
        cajaPago = (LinearLayout) rootView.findViewById(R.id.caja_pago);
        final Button bLimpiarDestino = (Button) rootView.findViewById(R.id.bLimpiarDestino);
        final Button bLimpiarOrigen = (Button) rootView.findViewById(R.id.bLimpiarOrigen);
        marcadorFijo = (ImageView) rootView.findViewById(R.id.marcadorFijo);
        cajaMarcador = (LinearLayout) rootView.findViewById(R.id.caja_marcador);
        botonMarcador = (Button) rootView.findViewById(R.id.bFijarMarcador);
        iGeneroPago = (ImageView) rootView.findViewById(R.id.iGeneroPago);

        bLimpiarDestino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etBuscarDestino.setText("");
            }
        });

        bLimpiarOrigen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etBuscarOrigen.setText("");
            }
        });

        etBuscarOrigen.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    buscandoDestino = false;
                    listaPuntos.setVisibility(View.VISIBLE);
                    fondo.setBackgroundColor(Color.parseColor("#d3d3d3"));
                    googleMap.setPadding(0,0,0,0);
                    cajaPago.setVisibility(View.GONE);
                    if (cajaMarcador.getVisibility() == View.VISIBLE) {
                        cajaMarcador.setVisibility(View.GONE);
                        marcadorFijo.setVisibility(View.GONE);
                    }
                }
            }
        });
        etBuscarDestino.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    acomodaCaja();
                    buscandoDestino = true;
                    listaPuntos.setVisibility(View.VISIBLE);
                    fondo.setBackgroundColor(Color.parseColor("#d3d3d3"));
                    googleMap.setPadding(0,0,0,0);
                    cajaPago.setVisibility(View.GONE);
                    if (marcadorInicial == null) {
                        ponerMarcadorEnPosicionInicial();
                    }
                    if (cajaMarcador.getVisibility() == View.VISIBLE) {
                        cajaMarcador.setVisibility(View.GONE);
                        marcadorFijo.setVisibility(View.GONE);
                    }
                }
            }
        });


        etBuscarOrigen.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (getActivity().getCurrentFocus() == etBuscarOrigen) {
                    buscandoDestino = false;
                    buscarLugar(charSequence);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if (etBuscarOrigen.getText().length() > 0) {
                    bLimpiarOrigen.setVisibility(View.VISIBLE);
                } else {
                    bLimpiarOrigen.setVisibility(View.GONE);
                }
            }

        });

        etBuscarDestino.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (getActivity().getCurrentFocus() == etBuscarDestino) {
                    buscandoDestino = true;
                    buscarLugar(charSequence);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if ( etBuscarDestino.getText().length()>0 ) {
                    bLimpiarDestino.setVisibility(View.VISIBLE);
                } else {
                    bLimpiarDestino.setVisibility(View.GONE);
                }
            }
        });


        listaPuntos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                fondo.setBackgroundColor(0);
                listaPuntos.setVisibility(View.GONE);
                myLocFab.setVisibility(View.VISIBLE);
                ocultarTeclado();
                if(i==0) { // Seleccionar en mapa
                    if (marcadorInicial != null) {
                        marcadorInicial.remove();
                    }
                    if (marcadorDestino != null) {
                        marcadorDestino.remove();
                    }
                    borraRuta();
                    if (buscandoDestino) {
                        if (latLngDestino != null) {
                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngDestino, 16));
                        }
                    } else {
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngOrigen, 16));
                    }
                    marcadorFijo.setVisibility(View.VISIBLE);
                    cajaMarcador.setVisibility(View.VISIBLE);
                } else {
                    obtenerCoordenadas(!buscandoDestino, direcciones.get(i-1).getId(), direcciones.get(i-1).getCalle());
                }
            }
        });

        botonMarcador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reponerMarcadores();
            }
        });



        selCon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), SeleccionarConductor.class));
            }
        });

        rlFormaPago.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), SeleccionarPago.class));
            }
        });

        cajaEmergencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cajaEmergencia.setVisibility(View.GONE);
                cajaPoli.setVisibility(View.VISIBLE);
            }
        });

        cajaPoliInterna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cajaPoli.setVisibility(View.GONE);
                cajaSwipe.setVisibility(View.VISIBLE);

                if (cochePorSolicitante != null) {
                    cochePorSolicitante.remove();
                } else {
                    Log.i(TAG, "coche no encontrado");
                }

                //animarDeNuevo();
                //paso=1;
                marcadorInicial.setIcon(BitmapDescriptorFactory.fromResource(
                        R.drawable.car
                ));
                marcadorInicial.setAnchor(0.5f, 0.5f);
                LatLng dest = new LatLng(directionPositionList.get(1).latitude,
                        directionPositionList.get(1).longitude);
                //prevRot = (float)angle(marcadorInicial.getPosition().latitude, marcadorInicial.getPosition().longitude,
                //        dest.latitude, dest.longitude);
                //marcadorInicial.setRotation(prevRot);
                //Log.i(TAG, "prevRot = " + prevRot);
                AnimadorMarcador a = new AnimadorMarcador(marcadorInicial, directionPositionList);
                a.animarDeNuevo();
                //paso++;

            }
        });

        return rootView;
    }

    private void ponerMarcadorEnPosicionInicial() {
        if (marcadorInicial == null) {
            latLngOrigen = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            colocarMarcador(true, latLngOrigen, dirOrigen);
            marcadorModificado = marcadorInicial;
            iniciarIntentDirecciones(latLngOrigen);
        }
    }

    private void reponerMarcadores() {
        cajaMarcador.setVisibility(View.GONE);
        marcadorFijo.setVisibility(View.GONE);
        colocarMarcador(true, latLngOrigen, dirOrigen);
        if (latLngDestino != null) {
            colocarMarcador(false, latLngDestino, dirDestino);
            dibujaRuta();
        }
    }

    //paso1
    private void acomodaCaja() {
        circuloAzul.setVisibility(View.VISIBLE);
        circuloRosa.setVisibility(View.VISIBLE);
        etBuscarOrigen.setVisibility(View.VISIBLE);
        tvADonde.setVisibility(View.GONE);
        lupa.setVisibility(View.GONE);

    }

    private void ocultarLista() {
        if (marcadorDestino != null && marcadorInicial != null) {
            circuloAzul.setVisibility(View.VISIBLE);
            lupa.setVisibility(View.GONE);
            circuloRosa.setVisibility(View.VISIBLE);
            // cajaPago.setVisibility(View.VISIBLE);
            ocultarTeclado();
        }
    }

    private void ocultarTeclado() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void buscarLugar(CharSequence charSequence) {
        cajaPago.setVisibility(View.GONE);
        final String value = charSequence.toString();
        if (charSequence.length() > 2) {
            fondo.setBackgroundColor(Color.parseColor("#d3d3d3"));
            listaPuntos.setVisibility(View.VISIBLE);
            myLocFab.setVisibility(View.GONE);
            // Quitar callbacks y mensajes
            mThreadHandler.removeCallbacksAndMessages(null);
            // Añadir nuevo
            mThreadHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Hilo de fondo
                    resultadosAutoComplete = mPlaceAPI.autocomplete(value, currentLocation);
                    direcciones = resultadosAutoComplete;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listaPuntos.setAdapter(new AdaptadorItemDireccion(getActivity(), direcciones));
                        }
                    });
                    // Post to Main Thread
                    mThreadHandler.sendEmptyMessage(1);
                }
            }, 500);
        } else if (charSequence.length() == 0) {
            fondo.setBackgroundColor(Color.TRANSPARENT);
            listaPuntos.setVisibility(View.GONE);
            myLocFab.setVisibility(View.VISIBLE);
        }
    }

    public void resetearCaja() {
        cajaPoli.setVisibility(View.GONE);
        fondo.setVisibility(View.VISIBLE);
        cajaPago.setVisibility(View.GONE);
        etBuscarOrigen.setText("");
        myLocFab.setVisibility(View.VISIBLE);
        etBuscarDestino.setText("");
        circuloAzul.setVisibility(View.GONE);
        circuloRosa.setVisibility(View.GONE);
        etBuscarOrigen.setVisibility(View.GONE);
        tvADonde.setVisibility(View.VISIBLE);
        lupa.setVisibility(View.VISIBLE);
        if (marcadorInicial != null) {
            marcadorInicial.remove();
            marcadorInicial = null;
        }
        if (marcadorDestino!= null) {
            marcadorDestino.remove();
            marcadorDestino = null;
        }
        marcadorModificado = null;
        dirDestino = null;
        dirOrigen = null;
        latLngOrigen = null;
        latLngDestino = null;
        googleMap.setPadding(0,0,0,0);
        esInicial = true;
        borraRuta();
    }

    private void obtenerCoordenadas(final Boolean origen, String id, final String direccion) {
        Places.GeoDataApi.getPlaceById(mGoogleApiClient, id)
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            if (origen) {
                                latLngOrigen = places.get(0).getLatLng();
                                dirOrigen = direccion;
                            } else {
                                latLngDestino = places.get(0).getLatLng();
                                dirDestino = direccion;
                            }
                            colocarMarcador(origen, places.get(0).getLatLng(), direccion);
                            if (marcadorDestino == null) {
                                googleMap.moveCamera(CameraUpdateFactory.newLatLng(places.get(0).getLatLng()));
                            } else {
                                dibujaRuta();
                            }
                        } else {
                            Log.e(TAG, "Place not found");
                        }
                        places.release();
                    }
                });
    }

    private void colocarMarcador(Boolean Origen, LatLng posicion, String direccion) {
        BitmapDescriptor btmp;
        if (Origen) {
            btmp = BitmapDescriptorFactory.fromResource(
                    R.drawable.mi_loc);
        } else {
            btmp = BitmapDescriptorFactory.fromResource(
                    R.drawable.destino);
        }
        Marker marcador = googleMap.addMarker(new MarkerOptions().position(posicion).
                title(direccion).draggable(false).icon(btmp
        ));
        if (Origen) {
            etBuscarOrigen.clearFocus();
            etBuscarOrigen.setText(direccion);
            dirOrigen = direccion;
            latLngOrigen = posicion;
            if (marcadorInicial != null) {
                marcadorInicial.remove();
            }
            marcadorInicial = marcador;
        } else {
            etBuscarDestino.clearFocus();
            etBuscarDestino.setText(direccion);
            dirDestino = direccion;
            latLngDestino = posicion;
            if (marcadorDestino != null) {
                marcadorDestino.remove();
            }
            marcadorDestino = marcador;
        }
    }

    private void zoomRuta() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(marcadorInicial.getPosition());
        builder.include(marcadorDestino.getPosition());
        bounds = builder.build();

        Resources r = getResources();
        float abajo = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 260, r.getDisplayMetrics());
        float arriba = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150, r.getDisplayMetrics());

        int padArriba = (int) arriba;
        int padAbajo = (int) abajo;
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 0);

        googleMap.setPadding(50, padArriba, 50, padAbajo);
        googleMap.animateCamera(cu);
    }

    private void dibujaRuta(){
        myLocFab.setVisibility(View.GONE);
        borraRuta();
        polArray.clear();
        GoogleDirection.withServerKey(serverKey)
                .from(marcadorInicial.getPosition())
                .to(marcadorDestino.getPosition())
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        Route route = direction.getRouteList().get(0);
                        Leg leg = route.getLegList().get(0);
                        directionPositionList = leg.getDirectionPoint();
                        numeroVueltas = directionPositionList.size();
                        for (int i = 0; i < numeroVueltas - 1; i++) {
                            String newColor = makeGradientColor(((float) i / (float) numeroVueltas) * 100);
                            PolylineOptions plo = new PolylineOptions().add(directionPositionList.get(i),
                                    directionPositionList.get(i + 1)).width(15).color(Color.parseColor(newColor));
                            polArray.add(googleMap.addPolyline(plo));
                        }
                        zoomRuta();

                        muestraTarifa();

                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        // Do something here
                    }
                });
    }

    public void borraRuta() {
        googleMap.setPadding(0,0,0,0);
        if(!polArray.isEmpty()) {
            for (Polyline line : polArray) {
                line.remove();
            }
        }
    }

    private void muestraTarifa() {
        myLocFab.setVisibility(View.GONE);
        ocultarTeclado();
        cajaPago.setVisibility(View.VISIBLE);
    }

    public void añadirCochePorSolicitante() {
        // añade coche que irá a recoger al solicitante
        Random r = new Random();
        double r1 = (r.nextDouble() - 0.5) / 25;
        double r2 = (r.nextDouble() - 0.5) / 25;
        double randomLat = marcadorInicial.getPosition().latitude + r1;
        double randomLon = marcadorInicial.getPosition().longitude + r2;
        LatLng randomPosition = new LatLng(randomLat, randomLon);
        cochePorSolicitante = googleMap.addMarker(new MarkerOptions().position(randomPosition).
                icon(BitmapDescriptorFactory.fromResource(R.drawable.car)).draggable(false));
        cochePorSolicitante.setAnchor(0.5f, 0.5f);
        GoogleDirection.withServerKey(serverKey)
                .from(randomPosition)
                .to(marcadorInicial.getPosition())
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        Route route = direction.getRouteList().get(0);
                        Leg leg = route.getLegList().get(0);
                        ArrayList<LatLng> al = leg.getDirectionPoint();

                        AnimadorMarcador am = new AnimadorMarcador(cochePorSolicitante, al);
                        am.animarDeNuevo();
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        // Do something here
                    }
                });
    }

    private String makeGradientColor(Float percent) {
        int r = makeChannel(74,215,  percent);
        int g = makeChannel(170,67,  percent);
        int b = makeChannel( 214,122, percent);

        String newColor = "#" +
                makeColorPiece(r) +
                makeColorPiece(g) +
                makeColorPiece(b);
        return newColor;
    }

    private int makeChannel(int a, int b, Float percent) {
        return (int) (a + Math.round(b - a) * (percent / 100));
    }

    private String makeColorPiece(int num) {
        num = Math.min(num, 255);
        num = Math.max(num, 0);
        String str = Integer.toString(num, 16);
        if (str.length() < 2) {
            str = "0" + str;
        }
        return str;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .build();
    }

    private void setUpMap() {
        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, LOCATION_PERMISSION_REQUEST_CODE);
            return;
        }

        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);

        LocationAvailability locationAvailability =
                LocationServices.FusedLocationApi.getLocationAvailability(mGoogleApiClient);

        if (locationAvailability != null) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (mLastLocation != null) {
                currentLocation = new LatLng(mLastLocation.getLatitude(),
                        mLastLocation.getLongitude());
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16));

                // Autos "fantasma"
                /*if (cochesRandom.size() < 1) {
                    Random r = new Random();
                    int numeroCoches = r.nextInt(6) + 1;
                    for (int i = 0; i < numeroCoches; i++) {
                        double r1 = (r.nextDouble() - 0.5) / 25;
                        double r2 = (r.nextDouble() - 0.5) / 25;
                        float giro = r.nextInt(360);
                        double randomLat = mLastLocation.getLatitude() + r1;
                        double randomLon = mLastLocation.getLongitude() + r2;
                        LatLng randomPosition = new LatLng(randomLat, randomLon);
                        final Marker m = googleMap.addMarker(new MarkerOptions().position(randomPosition).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.car)).
                                rotation(giro).draggable(false));
                        m.setAnchor(0.5f, 0.5f);
                        double r3 = (r.nextDouble() - 0.5) / 25;
                        double r4 = (r.nextDouble() - 0.5) / 25;
                        double randomLat2 = mLastLocation.getLatitude() + r3;
                        double randomLon2 = mLastLocation.getLongitude() + r4;
                        LatLng randomPosition2 = new LatLng(randomLat2, randomLon2);

                        GoogleDirection.withServerKey(serverKey)
                                .from(randomPosition)
                                .to(randomPosition2)
                                .execute(new DirectionCallback() {
                                    @Override
                                    public void onDirectionSuccess(Direction direction, String rawBody) {
                                        Route route = direction.getRouteList().get(0);
                                        Leg leg = route.getLegList().get(0);
                                        ArrayList<LatLng> al = leg.getDirectionPoint();

                                        AnimadorMarcador am = new AnimadorMarcador(m, al);
                                        am.animarDeNuevo();
                                    }

                                    @Override
                                    public void onDirectionFailure(Throwable t) {
                                        // Do something here
                                    }
                                });


                        cochesRandom.add(m);
                    }
                }*/

            }
        }
    }

    private Bitmap scaleImage(Resources res, int id, int lessSideSize) {
        Bitmap b = null;
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;

        BitmapFactory.decodeResource(res, id, o);

        float sc = 0.0f;
        int scale = 1;
        // if image height is greater than width
        if (o.outHeight > o.outWidth) {
            sc = o.outHeight / lessSideSize;
            scale = Math.round(sc);
        }
        // if image width is greater than height
        else {
            sc = o.outWidth / lessSideSize;
            scale = Math.round(sc);
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        b = BitmapFactory.decodeResource(res, id, o2);
        return b;
    }


    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                setUpMap();
            } else {
                // Mensaje de error. Los permisos fueron negados
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        setUpMap();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Conexión suspendida");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Conexión falló: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }


    @Override
    public void onSlideToUnlockCanceled() {}

    @Override
    public void onSlideToUnlockDone() {
        startActivity(new Intent(getActivity(), CalificaViaje.class));
        //caja5.setVisibility(View.GONE);
        cajaSwipe.setVisibility(View.GONE);
        myLocFab.setVisibility(View.VISIBLE);
        borraRuta();
        reiniciarMarcadores();
        resetearCaja();
    }

    public void reiniciarMarcadores() {
        marcadorInicial.remove();
        marcadorDestino.remove();
        marcadorInicial = null;
        marcadorDestino = null;
    }


    private void loadPlacePicker() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mThreadHandler != null) {
            mThreadHandler.removeCallbacksAndMessages(null);
            mHandlerThread.quit();
        }
    }

    /**
     * Creates an intent, adds location data to it as an extra, and starts the intent service for
     * fetching an address.
     */
    protected void iniciarIntentDirecciones(LatLng loc) {
        // Create an intent for passing to the intent service responsible for fetching the address.
        Intent intent = new Intent(getContext(), FetchAddressIntentService.class);

        // Pass the result receiver as an extra to the service.
        intent.putExtra(Constants.RECEIVER, mResultReceiver);

        // Pass the location data as an extra to the service.
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, loc);

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        getContext().startService(intent);
    }

    /**
     * Receiver for data sent from FetchAddressIntentService.
     */
    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }
        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            mAddressOutput = resultData.getString(Constants.RESULT_DATA_KEY);
            if (marcadorModificado != null) {
                //actualizarMarcador(marcadorModificado);
            } //else {
                String direccion = mAddressOutput.split(",")[0];
            if (esInicial) {
                etBuscarOrigen.clearFocus();
                etBuscarOrigen.setText(direccion);
                dirOrigen=direccion;
                esInicial=false;
                marcadorInicial.setTitle(direccion);
            } else if(buscandoDestino) {
                etBuscarDestino.clearFocus();
                etBuscarDestino.setText(direccion);
                dirDestino = direccion;
            } else {
                etBuscarOrigen.clearFocus();
                etBuscarOrigen.setText(direccion);
                dirOrigen=direccion;
                Log.i(TAG, dirOrigen);
            }
            //}

        }
    }

    protected void actualizarMarcador(Marker marcador) {
        String direccion = mAddressOutput.split(",")[0];
        marcador.setTitle(direccion);
        //marcador.showInfoWindow();
        /*if (marcador.getId().equals(marcadorInicial.getId())) {
            //tvADonde.setText(mAddressOutput);
            //direccionOrigen = mAddressOutput;
            //formatearInicio(direccionOrigen);
        } else {
            //tvDestino.setText(mAddressOutput);
            //formatearDestino(mAddressOutput);
            if (noPresionadoLargo == false) {
                dibujaRuta();
            }
        }
        noPresionadoLargo = true;*/
        if (marcadorDestino != null) {
            if (marcador.getId().equals(marcadorDestino.getId())) {
                dibujaRuta();
            } else {
                etBuscarOrigen.clearFocus();
                etBuscarOrigen.setText(direccion);
            }
        } else {
            etBuscarOrigen.clearFocus();
            etBuscarOrigen.setText(direccion);
            dirOrigen=direccion;
        }
        marcadorModificado = null;
    }

}