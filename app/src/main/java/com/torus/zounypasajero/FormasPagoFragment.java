package com.torus.zounypasajero;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;


/**
 * A simple {@link Fragment} subclass.
 */
public class FormasPagoFragment extends Fragment {


    public FormasPagoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_formas_pago, container, false);

        RelativeLayout agregarTarjeta = (RelativeLayout) rootView.findViewById(R.id.rlAgregarTarjeta);
        agregarTarjeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AgregarTarjeta.class));
            }
        });

        return rootView;
    }


}
