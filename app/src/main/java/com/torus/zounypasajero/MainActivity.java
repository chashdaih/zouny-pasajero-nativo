package com.torus.zounypasajero;

import android.Manifest;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private Timer timer;
    private DonutProgress donutProgress;
    private Fragment fragmentoPorAbrir = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Configuración de la dependencia para cambiar tipos de letra
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("Nunito-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        agregarToolbar();

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            prepararDrawer(navigationView);
            // Seleccionar item por defecto
            seleccionarItem(navigationView.getMenu().getItem(0));
        }

    }


    private void agregarToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            // Poner ícono del drawer toggle
            ab.setHomeAsUpIndicator(R.drawable.drawer_toggle);
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void prepararDrawer(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        item.setChecked(true);
                        seleccionarItem(item);
                        drawerLayout.closeDrawers();
                        return true;
                    }
                }
        );
    }

    private void seleccionarItem(MenuItem itemDrawer) {

        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (itemDrawer.getItemId()) {
            case R.id.item_inicio:
                fragmentoPorAbrir = new MapViewFragment();
                break;
            case R.id.item_formas:
                fragmentoPorAbrir = new FormasPagoFragment();
                break;
            case R.id.item_historial:
                fragmentoPorAbrir = new HistorialFragment();
                break;
            case R.id.item_ayuda:
                fragmentoPorAbrir = new AyudaFragment();
                break;
            case R.id.item_acerca:
                fragmentoPorAbrir = new AcercaFragment();
                break;
            case R.id.item_cerrar:
                break;
        }
        if (fragmentoPorAbrir != null) {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.contenedor_principal, fragmentoPorAbrir)
                    .commit();
        }

        // Setear título actual
        Log.i("Main Activity", itemDrawer.getTitle().toString());
        setTitle(itemDrawer.getTitle());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i("main", String.valueOf(item.getItemId()));
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public void mostrarOverlay(View v) {
        LinearLayout cajaPago = (LinearLayout) findViewById(R.id.caja_pago);
        LinearLayout cajaArriba = (LinearLayout) findViewById(R.id.fondo);
        RelativeLayout overlay = (RelativeLayout) findViewById(R.id.requesting);
        TextView tvRecogerAqui = (TextView) findViewById(R.id.tvRecogerAqui);
        MapViewFragment mvf = (MapViewFragment) fragmentoPorAbrir;
        Marker marcadorInicial = mvf.marcadorInicial;
        GoogleMap googleMap = mvf.googleMap;
        LatLngBounds bounds = mvf.bounds;

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, 1);
            return;
        }
        mvf.borraRuta();
        tvRecogerAqui.setText(mvf.direccionOrigen);
        googleMap.setMyLocationEnabled(false);
        cajaArriba.setVisibility(View.GONE);
        cajaPago.setVisibility(View.GONE);
        overlay.setVisibility(View.VISIBLE);

        /*Resources r = getResources();
        int padLeft = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 125, r.getDisplayMetrics());
        int padTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, r.getDisplayMetrics());
        int padBottom = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 210, r.getDisplayMetrics());

        googleMap.setPadding(padLeft,padTop,padLeft,padBottom);
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 0);
        googleMap.animateCamera(cu);*/
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(marcadorInicial.getPosition(), 17.0f);
        googleMap.animateCamera(yourLocation);

        animarCirculo();
    }

    public void ocultarOverlay(View v) {
        RelativeLayout cajaPoli = (RelativeLayout) findViewById(R.id.caja_poli);
        RelativeLayout overlay = (RelativeLayout) findViewById(R.id.requesting);

        MapViewFragment mvf = (MapViewFragment) fragmentoPorAbrir;
        for (Marker m : mvf.cochesRandom) {
            m.remove();
        }
        mvf.cochesRandom.clear();
        mvf.añadirCochePorSolicitante();

        GoogleMap googleMap = mvf.googleMap;
        googleMap.getUiSettings().setAllGesturesEnabled(true);
        googleMap.setPadding(0,0,0,0);

        LatLng posMarcadorInicial = mvf.marcadorInicial.getPosition();

        CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(posMarcadorInicial, 14);
        googleMap.animateCamera(cu);

        cajaPoli.setVisibility(View.VISIBLE);
        overlay.setVisibility(View.GONE);
    }


    public void animarCirculo() {
        donutProgress = (DonutProgress) findViewById(R.id.donut_progress);
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        boolean a = false;
                        /*if(a) {
                            ObjectAnimator anim = ObjectAnimator.ofInt(donutProgress, "progress", 0, 10);
                            anim.setInterpolator(new DecelerateInterpolator());
                            anim.setDuration(500);
                            anim.start();
                        } else {*/
                            AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.progress_anim);
                            set.setInterpolator(new DecelerateInterpolator());
                            set.setTarget(donutProgress);
                            set.start();
                        //}
                    }
                });
            }
        }, 0, 20000);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public void onBackPressed() {
        MapViewFragment mvf = (MapViewFragment) fragmentoPorAbrir;
        ListView listaPuntos = (ListView) findViewById(R.id.lista_dir);
        LinearLayout fondo = (LinearLayout) findViewById(R.id.fondo);
        LinearLayout cajaPago = (LinearLayout) findViewById(R.id.caja_pago);
        View circuloAzul = findViewById(R.id.circulo_azul_nuevo);
        List<Polyline> polArray = mvf.polArray;

        if (listaPuntos.getVisibility() == View.VISIBLE) {
            listaPuntos.setVisibility(View.GONE);
            fondo.setBackgroundColor(Color.TRANSPARENT);
            if (polArray.size()>0) { // si hay ruta trazada
                cajaPago.setVisibility(View.VISIBLE);
            }
        /*}  else if (cajaPago.getVisibility() == View.VISIBLE){
            mvf.resetearCaja();*/
        }  else if (circuloAzul.getVisibility() == View.VISIBLE) {
            cajaPago.setVisibility(View.GONE);
            mvf.resetearCaja();
        } else {
            super.onBackPressed();
        }
    }
}
